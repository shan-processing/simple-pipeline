#!/usr/bin/env bash

functions=$(dirname $0)/functions.sh
source $functions

if [ -z $COLORMAP ]; then
    COLORMAP=$(dirname $0)/colormap.txt
fi

function help {
    echo "Usage: report_pipeline.sh -i IMAEG -d DATA_DIR [-a ALPHA -n NUM_SLICES]"
    echo ""
    echo "Generate cerebellum parcellation report. Convert the image into .png and montage"
    echo "them together. The .png files are stored in DATA_DIR/.png, the montage of the"
    echo "intermediate steps is in DATA_DIR/pics/intermediate/, and the montage of the"
    echo "final result is in DATA_DIR/pics/final/"
    echo ""
    echo "Args:"
    echo "    -i IMAGE: The original image"
    echo "    -d DATA_DIR: The data directory."
    echo "    -a ALPHA (1): The alpha channel of the overlay."
    echo "    -n NUM_SLICES (10): The number of slices to show."
    echo "    -h Print this help message."
}

num_slices=10
alpha=1

while getopts ":hi:d:a:n:" opt; do
    case $opt in
        h)
            help
            exit 0
            ;;
        i)
            image=$OPTARG
            ;;
        d)
            data_dir=$OPTARG
            ;;
        a)
            alpha=$OPTARG
            ;;
        n)
            num_slices=$OPTARG
            ;;
        \?)
            echo "Invalid option: $OPTARG"
            help
            exit 1
            ;;
    esac
done

echo Report generation...

png_dir=$data_dir/.png
intermediate_dir=$data_dir/pics/intermediate
final_dir=$data_dir/pics/final
mkdir -p $png_dir $intermediate_dir $final_dir

mni_image=$(add_nii_suffix $image n4_mni true $data_dir/mni)
parc=$(add_nii_suffix $mni_image seg true $data_dir/parc)
post=$(add_nii_suffix $parc post true $data_dir/parc)
inverse=$(add_nii_suffix $post inverse true $data_dir)

function get_filename() {
    basename $1 | sed "s/\.gz//" | sed "s/\.nii//"
}

function calc_indices() {
    local start=${1}.0
    local stop=${2}.0
    local indices=$(python -c "import numpy as np; print(np.round(np.linspace($start, $stop, $num_slices)).astype(int))")
    echo $indices | sed "s/^\[//" | sed "s/\]$//"
}

# find usable fonts for montage
for font in $(convert -list font | grep glyphs | sed "s,[^/]*,,"); do
    if [ -f $font ]; then
        break
    fi
done

views=(axial coronal sagittal)
for view in ${views[@]}; do
    mni_output_dir=$png_dir/$(get_filename $mni_image)/$view
    convert_nii -i $mni_image -v $view -q -l -o $mni_output_dir
    parc_output_dir=$png_dir/$(get_filename $parc)/$view
    convert_nii -p $mni_image $parc -v $view -q -l -a $alpha -o $parc_output_dir -c $COLORMAP
    post_output_dir=$png_dir/$(get_filename $post)/$view
    range=$(convert_nii -p $mni_image $post -v $view -q -l -a $alpha -o $post_output_dir -c $COLORMAP -s)

    indices=($(calc_indices $range))
    images=()
    for ind in ${indices[@]}; do
        im=($(ls $mni_output_dir/*${ind}.png))
        images+=($im)
        im=($(ls $parc_output_dir/*${ind}.png))
        images+=($im)
        im=($(ls $post_output_dir/*${ind}.png))
        images+=($im)
    done
    output_pic=$intermediate_dir/${view}.png
    montage ${images[@]} -font $font -geometry +0+0 -tile 3x$num_slices $output_pic

    im_output_dir=$png_dir/$(get_filename $image)/$view
    convert_nii -i $image -v $view -q -l -o $im_output_dir
    inv_output_dir=$png_dir/$(get_filename $inverse)/$view
    range=$(convert_nii -p $image $inverse -v $view -q -l -a $alpha -o $inv_output_dir -c $COLORMAP -s)

    indices=($(calc_indices $range))
    images=()
    for ind in ${indices[@]}; do
        im=($(ls $im_output_dir/*${ind}.png))
        images+=($im)
        im=($(ls $inv_output_dir/*${ind}.png))
        images+=($im)
    done
    output_pic=$final_dir/${view}.png
    montage ${images[@]} -font $font -geometry +0+0 -tile 2x$num_slices $output_pic
done
