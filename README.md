# Simple Processing Pipeline Using Bash

The processing takes a data directory in BIDS and performs:

1. N4
2. ROBEX skull strip
3. Weighted N4
4. MNI
5. Cerebellum parcellation
6. Post processing

Each step will be stored in separate directories and corresponding images are soft-linked
