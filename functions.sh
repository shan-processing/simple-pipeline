function add_nii_suffix() {
    local image=$1
    local suffix=$2
    local gz=$3 # true or false
    local output_dir=$4
    local image=$(echo $image | sed "s/\.gz$//" |sed "s/\.nii$/_${suffix}.nii/")
    if $gz; then
        local image=${image}.gz
    fi
    if ! [ -z $output_dir ]; then
        local image=$output_dir/$(basename $image)
    fi
    echo $image
}
